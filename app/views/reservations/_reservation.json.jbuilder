json.extract! reservation, :id, :name, :Book, :Librian, :Client, :created_at, :updated_at
json.url reservation_url(reservation, format: :json)
