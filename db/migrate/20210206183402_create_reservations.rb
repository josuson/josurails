class CreateReservations < ActiveRecord::Migration[5.2]
  def change
    create_table :reservations do |t|
      t.reference :name
      t.reference :Book
      t.reference :Librian
      t.reference :Client

      t.timestamps
    end
  end
end
