class CreateBooks < ActiveRecord::Migration[5.2]
  def change
    create_table :books do |t|
      t.string :name
      t.reference :Stock
      t.reference :Category
      t.reference :Author

      t.timestamps
    end
  end
end
